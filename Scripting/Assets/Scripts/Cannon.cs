﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public GameObject Bullet;
    public GameObject Gun;
    public float ShotsPerSecond;
    public float Force;

    private float timeOfLastShot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        // if the mouse button is held down AND the time since last shot is larger than the time between shots according to ShotsPerSecond
        if (Input.GetMouseButton(0) && (Time.time - timeOfLastShot) > (1f/ShotsPerSecond))
        {
            // determine aim point position
            Ray rayMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 position = rayMouse.GetPoint(1000);

            // spawn bullet at location of gun
            GameObject newBullet = Instantiate(Bullet, Gun.transform.position, Quaternion.identity);
            
            // Determine direction and apply force to create projectile movement
            Vector3 shoot = (position - newBullet.transform.position).normalized;
            newBullet.GetComponent<Rigidbody>().AddForce(shoot * Force);

            // store time of last shot
            timeOfLastShot = Time.time;
        }

    }



}
